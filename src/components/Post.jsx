import React from 'react';

const Post = (props) => {
    return (
        <div>
            <li>{props.post.title}</li>
            <a onClick={() => {props.onEdit(props.post.id)}}>Edit</a>
            <a className="has-text-danger" onClick={() => {props.onDelete(props.post.id)}}>Delete</a>
        </div>
    );
};

export default Post;
