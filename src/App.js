import React, { useState, useEffect } from 'react';
import './bulma.min.css';
import axios from 'axios'
import Post from "./components/Post";

function App() {
    const [posts, setPosts] = useState([])

    const [title, setTitle] = useState('')
    const [body, setBody] = useState('')

    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then((response) => {
                setPosts(response.data)
            });
    }, []);

    const handleEvent = e => {
        e.preventDefault()
        setPosts([...posts, {title, body}])
        setTitle('')
        setBody('')
    }

    const onEdit = (id) => {
        let post = posts.filter(element => element.id === id)

        console.log(post)
        setTitle(post[0].title)
        setBody(post[0].body)
    }

    const onDelete = (id) => {
        let newPosts = posts.filter(element => element.id !== id)

        setPosts(newPosts)
    }

    return (
      <div>
        <section className="section">
          <div className="container">
              <form onSubmit={handleEvent}>
                  <input type="text" value={title} onChange={e => {setTitle(e.target.value)}}/>
                  <textarea
                      value={body}
                      onChange={e => {setBody(e.target.value)}}
                      cols="30"
                      rows="10">
                  </textarea>
                  <button className="button is-primary">submit</button>
              </form>

              <ul>
                  {posts.map((post) => {
                      return <Post key={post.id} post={post} onEdit={onEdit} onDelete={onDelete}/>
                  })}
              </ul>
          </div>
        </section>
      </div>
    );
}

export default App;